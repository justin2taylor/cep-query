/**
* CEP Query 0.0.4
* Moderator: Justin Taylor
* Contributors: Justin Taylor 
* 
* Description: A general purpose library for Adobe CEP Panels 
* 
* @requires Node.js
* @requires Vulcan
* @requires CSInterface
*/

/**
 * Initiate CEP-Query Namespace
 */
var cq = {};


/**
* Evaluates Extendscript and Returns Error if thrown
* Credit: Justin Taylor
* 
* @param {!String} script ExtendScript Code to be executed
* @return {!Promise(resolve)} Returns promise with funciton result or error message
*/
cq.evalES = function (script) {
	var csi = new CSInterface();
	return new Promise(function (resolve, reject) {
		csi.evalScript('try{' + script + '}catch(e){e}', resolve);
	});
};
/**
* Registers every possible key command allowed by app
* Credit: Justin Taylor
* 
* @param {!String} script ExtendScript Code to be executed
* @return {!Promise(resolve)} Returns promise with funciton result or error message
*/
cq.keyRegisterOverride = function () {
	var os = navigator.platform.substr(0, 3);
	var maxKey;
	if (os === 'Mac') {
		maxKey = 126; // Mac Max Key Codes   
	}
	else if (os === 'Win') {
		maxKey = 222; // HTML Max Key Codes
	}
	var allKeys = [];
	for (var k = 0; k <= maxKey; k++) {
		for (var j = 0; j <= 15; j++) {
			var guide = (j >>> 0).toString(2).padStart(4, '0');
			allKeys.push({
				keyCode: k,
				ctrlKey: guide[0] == 1,
				altKey: guide[1] == 1,
				shiftKey: guide[2] == 1,
				metaKey: guide[3] == 1
			});
		}
	}
	var csi = new CSInterface();
	var keyRes = csi.registerKeyEventsInterest(JSON.stringify(allKeys));
	console.log('Key Events Registered Completed: ' + keyRes);
};
/**
* Prevents dragging of external URLs into the panel
* Credit: Justin Taylor
*/
cq.dropDisable = function () {
	window.addEventListener('dragover', function (e) {
		e = e || event;
		e.preventDefault();
	}, false);
	window.addEventListener('drop', function (e) {
		e = e || event;
		e.preventDefault();
	}, false);
};
/**
* Map value from one range to another
* Credit: Justin Taylor
* 
* @param {!Number} num current value
* @param {!Number} in_min input minimum value
* @param {!Number} in_max input maximum value
* @param {!Number} out_min output minimum value
* @param {!Number} out_max output maximum value
* @return mapped value
*/
cq.rangeMap = function (num, in_min, in_max, out_min, out_max) {
	return (num - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
};
/**
* Parse all Command IDs for After Effects from dictionary file
* Credit: Justin Taylor
* 
* @return Menu ID object
*/
cq.cmdIDs = function () {
	return new Promise(function (resolve, reject) {
		var stream = require('stream');
		var path = require('path');
		var fs = require('fs');
		var readline = require('readline');
		cq.evalES('Folder.appPackage.fsName').then(function (appPackage) {
			appPackage = decodeURI(appPackage);
			var datFile = path.join(appPackage, '/Dictionaries/es_ES/after_effects_es_ES.dat');
			console.log(datFile);
			var instream = fs.createReadStream(datFile, 'utf16le');
			var outstream = new stream;
			var rl = readline.createInterface(instream, outstream);
			var menuIDs = {};
			rl.on('line', function (line) {
				if (line.match(/\$\$\$\/AE\/MenuID/)) {
					line = line.substring(line.lastIndexOf('/') + 1);
					var data = line.match(/[a-z|A-Z|0-9]*\_[0-9]*/);
					if (data) {
						var name = data[0].match(/[a-z|A-Z|0-9]*\_/)[0].slice(0, -1);
						var number = data[0].match(/\_[0-9]*/)[0].substr(1);
						menuIDs[number] = name;
					}
				}
			});
			rl.on('close', function () {
				resolve(menuIDs);
			});
		});
	});
};

/**
* Get the file extension for a Media Encoder export preset file
*
* Note: AfterCodecs shares a FileType number for both mov and mp4, 
* so afterCodecsExtension() drills down to the extension param.
* 
* Credit: Justin Taylor
* 
* @param {!String} path path to EXR file
* @return {!String} file extension
*/
cq.getEprExtension = function (eprPath) {
	var fs = require('fs');

	var txt = fs.readFileSync(eprPath, 'utf8');
	var xml = (new DOMParser()).parseFromString(txt, 'text/xml');
	var element = xml.getElementsByTagName('ExporterFileType')[0];
	var value = element.textContent;

	function afterCodecsExtension(xml) {
		var acExts = {
			1: 'mp4',
			2: 'mov',
		};
		var val = xml.ExporterParam[4].ParamValue;
		return acExts[val];
	}

	var eprList = {
		1299148630: "mov", // Quicktime
		1211250228: "mp4", // H.264
		1094796064: "aac", // AAC
		1095321158: "aif", // AIFF
		1297625392: "mxf", // AS-10
		1297625393: "mxf", // AS-11
		1145919558: "mxf", // DNxHD
		1146116128: "dpx", // DPX
		1211250242: "m4v", // H.264 Blu Ray
		1212503619: "mp4", // HVEC H.265
		1246774599: "jpg", // JPEG Sequence
		1246582854: "mxf", // JPEG 2000 Sequence
		1685480480: "m2v", // MPEG-2 DVD
		1297101600: "mp3", // MP3
		1297101856: "3gp", // MPEG-4
		1835164704: "m2v", // MPEG-2 Blu Ray
		1836082994: "mpg", // MPEG-2
		1297630808: "mxf", // MXF OPa1
		1866815570: "exr", // Open EXR
		1297630752: "MXF", // P2 Movie
		1347307296: "png", // PNG Sequence
		1414547779: "tga", // Targa
		1414088262: "tif", // TIFF
		1463899717: "wav", // Waveform Audio
		1145262175: "dcp", // Wraptor DCP
		1396984671: 'mov', // AfterCodecs
	}
	var res = value === 1396984671 ? afterCodecsExtension(xmlContents) : eprList[value];
	return res;
}


/**
* New way to run extendscipt commands directly
* Credit: Justin Taylor
*
* The all-new ExtendScript module es
* All you have to do is add any extendscript function call after 'es.'
* If you don't wanna return a function, then add '._()' at the end
* A promise is returned that you can act on after with '.then()'
* 
*/
if (typeof Proxy !== 'undefined') {
	var proxyGetter = function (target, prop, receiver) {
		if (target.main)
			esCall = prop;
		else if (prop == '_') {
			esFunction = false;
			return new Proxy(function () { }, { get: proxyGetter, apply: proxyCaller });
		}
		else
			esCall += '.' + prop;
		if (target[prop] === undefined) {
			target[prop] = new Proxy(function () { }, { get: proxyGetter, apply: proxyCaller });
			return target[prop];
		}
		else
			return target[prop];
	};
	var proxyCaller = function (target, thisArg, argumentsList) {
		var call = esCall;
		if (argumentsList.length > 0)
			call += '(' + JSON.stringify(argumentsList).slice(1, -1) + ')';
		else if (esFunction === true)
			call += '()';
		// console.log('Ima call ' + call);
		esFunction = true;
		return cq.evalES(call);
	};

	var es = new Proxy(function () { }, { get: proxyGetter, apply: proxyCaller });
	es.main = true;
	var esCall = '';
	var esFunction = true;
}